#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <string.h>

void Transpose_Block (int size, int dim, int dimreal, double  A[dimreal][dimreal])
{
    int i,j,mayorfila, mayorcol;
    register int Temp,k,l;

    for ( i = 0; i < dim; i += size)
    {
     mayorfila = ((i + size)>=dim)?dim:(i + size);
     for ( j = 0; j < i; j += size)
     {
        mayorcol  = ((j + size)>=dim)?dim:(j + size);
        for ( k = i; k < mayorfila; ++k)
        {
            for ( l = j; l < mayorcol; ++l)
            {
                 Temp = A[k][l];
                 A[k][l]  =  A[l][k];
                 A[l][k]  = Temp;

            }
        }
     }
     mayorfila = ((i + size-1)>=dim)?dim:(i + size-1);
     mayorcol  = ((i + size)>=dim)?dim:(i + size);
     for ( k = i; k < mayorfila; ++k)
      for ( l = k+1; l < mayorcol; ++l)
            {
                 Temp = A[k][l];
                 A[k][l]  =  A[l][k];
                 A[l][k]  = Temp;            }
   }
}
#define DIM 4104

double A[DIM][DIM] __attribute__((aligned(64)));
double B[DIM][DIM] __attribute__((aligned(64)));

void main(int argc, char **argv){

   //double *matriz=0;

   int i,j,fr;

   //int dimension = atoi(argv[1]);
   int dimension = 4096;
   int tamano_bloque = atoi(argv[1]);
   int TAMLINE = 64/sizeof(double);
   //int dimReal = (dimension%TAMLINE == 0)? dimension : floor((float)dimension/TAMLINE)*TAMLINE+TAMLINE;
   float freq = (float)CLOCKS_PER_SEC;
   //if ((int)floor((float)dimReal/TAMLINE)%2 ==0) dimReal+=TAMLINE;
   int dimReal = DIM;
  long num_iters = 100;  //100
  int  r,s;
  double suma=0, sumalocal=0,maximolocal,minimolocal,minimo,maximo;
  clock_t fin,ini;
  for (r=0;r<DIM;r++)
     for (s=0;s<DIM;s++)
        B[r][s]+=0;
   for ( j= 0; j < 4; j++){  //4
      for ( i=0;i <num_iters; i++)
      {
          for (r=0;r<DIM;r++)
           for (s=0;s<DIM;s++)
              B[r][s]+=(j+i)%100;

          ini = clock();
         Transpose_Block(tamano_bloque, dimension, dimReal, A);
          fin = clock();
          if (i==0) sumalocal=minimolocal=maximolocal=(fin-ini);
          else
          {
              if (minimolocal>(fin-ini)) minimolocal=(fin-ini);
              if (maximolocal<(fin-ini)) maximolocal=(fin-ini);
              sumalocal+=(fin-ini);
          }
          if (j==0 && i==0) minimo=maximo=(fin-ini);
          else
          {
              if (minimo>(fin-ini)) minimo=(fin-ini);
              if (maximo<(fin-ini)) maximo=(fin-ini);
          }
          suma+=(fin-ini);
     }
        // printf("blo %d time minimo %f maximo %f suma %f\n",tamano_bloque,(float)minimolocal/(float)freq,(float)maximolocal/(float)freq,(float)sumalocal/(float)freq);
    }
printf("blo %d time glo minimo %f maximo %f media %f\n",tamano_bloque,(float)minimo/(float)freq,(float)maximo/(float)freq,((float)suma/(float)(num_iters*4))/(float)freq);
    //printf("1000 B %f %f %f %f\n",B[0][0],B[1024][100],B[1000][1024],B[4095][4095]);

//clock_t start, end;
//double cpu_time_used;
//start = clock();
//end = clock();
//cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

}

